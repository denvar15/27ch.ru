import {
    GET_THEME,
    RESET_THEME_BLACK,
    RESET_THEME_DEF
} from '../actions';

const intialState = {
    theme: '2019',
};

const theme_default = '2018';
const theme_black = '2019';


export default function(state=intialState,action){
    switch (action.type) {
        case GET_THEME:
            return {...state};
        case RESET_THEME_BLACK:
            return {...state,theme: theme_black};
            break;
        case RESET_THEME_DEF:
            return {...state,theme: theme_default};
            break;
    }
    return state;
}