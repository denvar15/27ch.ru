import {ERROR} from "./index";

export const AUTH_USER = 'auth_user';
export const AUTH_ERROR = 'auth_error';

export const SINGUP_USER = 'signup_user';
export const SINGUP_ERROR = 'signup_error';

export const GET_USER = 'get_user';
export const GET_POST_USER = 'get_post_user';
export const DISGET_USER = 'disget_user';

export const UNAUTH_USER = 'unauth_user';

export const LOGIN_BY_FB = 'login_by_fb';

export const EDITING_USER = 'EDITING_USER';
export const EDITED_USER = 'EDITED_USER';