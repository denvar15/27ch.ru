import axios from 'axios';
import {
	AUTH_USER,
	UNAUTH_USER,
	SIGNUP_USER,
	AUTH_ERROR,
    GET_USER,
    DISGET_USER,
	EDITING_USER,
	EDITED_USER,
    GET_POST_USER
} from '../types';
import {EDITED_POST, EDITING_POST, ERROR, FETCHED_BLOGS, FETCHED_POST, FETCHING_BLOGS, FETCHING_POST} from "../index";
import {tokenHeader} from "../../utils/headers";

const ROOT_URL = 'https://pacific-falls-52582.herokuapp.com/accounts/api/';

export function signup(formValue,callback){
	const URL = `${ROOT_URL}register/`;
	return (dispatch) =>{
		axios.post(URL, formValue)
		.then((response)=>{
			const{username}= response.data;
			window.location.href = "/";
			dispatch({type:SIGNUP_USER});
			localStorage.setItem('token',response.data.token);
			localStorage.setItem('username',username);

		})
		.catch((error)=>{
			dispatch({type:AUTH_ERROR,payload:'ERROR OCCURED USERNAME MAY EXISTS IN DATABASE'});
		})
	}
}

export function imagePostUser(fromValue,id,callback){
	let formData = new FormData();
  	formData.append('image', fromValue);
	const sub_url = `update/${id}/`;
	const url = `${ROOT_URL}${sub_url}`;
	const id_patch = localStorage.getItem('id');
	const url_patch =`${ROOT_URL}update/now_user/${id_patch}/`;
	const url_get =`${ROOT_URL}get/now_user/${id}/`;
	const request = axios.patch(url,formData,tokenHeader());
	return (dispatch) =>{
		dispatch({type:EDITING_USER});
		request.then((response)=>{
			dispatch({type:EDITED_USER});
			callback();
			let response1 = response;
			axios.get(url_get, tokenHeader())
				.then((response)=>{
					console.log(response);
					let data = {};
					data.email = response.data.email;
		    		data.UID = response.data.UID;
		    		data.username = response.data.username;
		    		data.type = response.data.type;
		    		data.image = response1.data.image;
		    		axios.patch(url_patch, data, tokenHeader())
                	.then((response)=> {
                    	callback();
                    	window.location.href = '/profile/'
                	})
				})
		});
	}
}

export function get_nowuser(id1){
    let id = 0;
	if (id1 === undefined) {
		id = localStorage.getItem('id');
	} else {
		id = id1
	}
	const url =`${ROOT_URL}get/now_user/${id}/`;

	const request = axios.get(url, tokenHeader());

	return (dispatch) =>{
		request.then((response)=>{
			dispatch({type:GET_USER,payload: response.data});
		})
		.catch((err)=>{
			dispatch({type:ERROR,payload:err});
		})
	};
}

export function get_post_nowuser(id1){
    let id = 0;
    if (id1 === undefined) {
        id = localStorage.getItem('id');
    } else {
        id = id1
    }
    const url =`${ROOT_URL}get/now_user/${id}/`;

    const request = axios.get(url, tokenHeader());

    return (dispatch) =>{
        request.then((response)=>{
            dispatch({type:GET_POST_USER,payload: response.data});
        })
            .catch((err)=>{
                dispatch({type:ERROR,payload:err});
            })
    };
}


export function signin(formValue,callback){
	const URL =`${ROOT_URL}home/login/token/`;
	const url_new =`${ROOT_URL}create/now_user/`;
	return (dispatch) => {
	    axios.post(URL,formValue)
			.then((response)=>{
				const {username} = response.data.user;
				console.log(response.data.user);
				dispatch({type: AUTH_USER, payload: response.data.user});
				localStorage.setItem('token', response.data.token);
				localStorage.setItem('username', username);
				localStorage.setItem('id',response.data.user.id);

				let data = {};
				data.email = response.data.user.email;
				data.UID = response.data.user.id;
				data.username = response.data.user.username;
				data.type = response.data.user.type;
				data.image = response.data.user.image;

				const id = localStorage.getItem('id');
				const url =`${ROOT_URL}get/now_user/${id}/`;
				const request = axios.get(url, tokenHeader());
				request.then((response)=>{
                    callback();
                    window.location.href = '/'
				})
					.catch((err)=>{
						axios.post(url_new, data)
							.then((response)=> {
								const url_vk = 'https://pacific-falls-52582.herokuapp.com/vk/';
								let data = {};
								data.username = username;
								axios.post(url_vk, data, tokenHeader())
									.then((response) => {
										console.log('VK_BOT_START');
										callback();
									});
								window.location.href = '/'
							})
					});
		})
			.catch((err)=>{
				dispatch({type:AUTH_ERROR,payload:'BAD LOGIN CREDENTIALS'});
			})
	}
}

export function signout(callback){
	localStorage.removeItem('token');
	localStorage.removeItem('username');
	return (dispatch) =>{
		dispatch({type:UNAUTH_USER});
		dispatch({type:DISGET_USER});
		callback();
	}
}