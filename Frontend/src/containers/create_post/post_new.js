import React,{Component,PropTypes} from "react";
import {Field,reduxForm} from "redux-form";
import {connect} from "react-redux";
import {createPost, getBlogs, imagePost} from "../../actions/index";
import {Link} from "react-router-dom";
import FieldFileInput from './image_adding/fileUpload'

import '../../css/postnew.css'

class PostNew extends Component{

	find = (array) => {
		let maximum = 0;
		for (let i = 0; i < array.length; i++) {
			if (array[i].id > maximum) {
                maximum = array[i].id;
            }
		}
		return maximum
	};
    componentWillMount(){
        this.props.theme.theme = localStorage.getItem('theme');
    }

	onSubmit(formValue){
		let data = {};
		data.type = formValue.type;
	    data.title = formValue.title;
	    data.respondent = 'no respondent';
		data.content = formValue.content;
		data.PID = this.props.auth.user.UID;
		console.log('A', data);
		this.props.createPost(data, ()=>{
			this.props.getBlogs();
			if (this.props.blogs.isFetched===true) {
				let posts = this.props.blogs.posts;
				const maximum = this.find(posts) + 1;
				console.log('AAA', maximum);
				this.props.imagePost(formValue.image, maximum, ()=>{
					//window.location.href = "/posts/";
					console.log('3 phase')
				})

			}
		});
	}

	render(){
		const {fields:{title,content},handleSubmit} = this.props;
		return(
			<div style = {this.props.theme.theme === '2018' ?  {}: {color: 'rgb(248, 205, 6)'}} className="columns">
				<div className="column col-4"> </div>
				<div className="column col-4">
					<form onSubmit = {handleSubmit(this.onSubmit.bind(this))}>
						<div className="form-group">
							<label className="form-label">Заголовок</label>
							<Field style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)', color: 'rgb(240, 244, 244)'}} component="input"  name="title" type="text" className="form-input" placeholder="Textarea"/>
						</div>
						 <div className="form-group">
						    <label className="form-label">Текст</label>
						    <Field style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)', color: 'rgb(240, 244, 244)'}} component="textarea" name="content" className="form-input" id="input-example-3" rows="3" placeholder="Textarea"/>
						 </div>
						<div className="form-group">
						    <label className="form-label">Тема</label>
						    <Field style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)', color: 'rgb(240, 244, 244)'}} component="textarea" name="type" className="form-input" id="input-example-3" placeholder="Textarea" rows="1"/>
						 </div>
						<div className='form-group'>
							<Field component={FieldFileInput} name='image'/>
						</div>
						 <div className="form-group">
						 	<button className={this.props.theme.theme === '2018' ? "button-view" : 'Dark-button-view'} type="submit" id="vini">Отправить</button>
						 	<Link to="/posts/" className={this.props.theme.theme === '2018' ? "button-back" : 'Dark-button-back'}>Отмена</Link>
						 </div>
					</form>
				</div>
			</div>
		);
	}
}

//redux form almost similar to connect 1st parameter is form object 2nd is mapStateToProps 3rd is mapDispatchToProps.
//we will use shorthand of mapDispatchToProps ,We could have written mapDispatchToProps function and then use bindActionCreators to map dispatch to props .but instead of doing all that we just pass the function as argument in reduxForm.
function mapStateToProps(state){
	return {
		newpost:state.newpost,
		blogs:state.blogs,
		auth:state.auth,
        theme: state.theme
	}
}

export default connect(mapStateToProps, {createPost, getBlogs, imagePost})(reduxForm({
	form:'PostForm',
	fields:['title','content', 'type', 'respondent'],
})(PostNew));