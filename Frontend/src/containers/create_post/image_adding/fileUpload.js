import React, {Component} from 'react'
import '../../../css/postnew.css'

export default class FieldFileInput  extends Component{
  constructor(props) {
    super(props)
    this.onChange = this.onChange.bind(this)
  }

  onChange(e) {
    const { input: { onChange } } = this.props
    onChange(e.target.files[0])
  }

  render(){
    const { input: { value } } = this.props
    const {input,label, required, meta, } = this.props  //whatever props you send to the component from redux-form Field
      console.log('A', input.value.name);
    return(
     <div>
     <div>
         <label style={{cursor: 'pointer'}} htmlFor='file'>Изображение - <i className="fas fa-upload fa-2x"> </i></label>
       <input id='file' name='file' className='inputfile'
        type='file'
        accept='.jpg, .png, .jpeg'
        onChange={this.onChange}
       />
         <p style={{marginLeft: '1vw', display:'inline'}}>{input.value.name}</p>
     </div>
     </div>
    )
}
}