import React,{Component} from "react";
import {Link} from 'react-router-dom';
import {changeMode, deletePost} from '../../actions/index';
import {connect} from "react-redux";

import '../../css/typelist.css'

class News extends Component{

	renderNews = (news) => {
	    return(
	        <div style = {this.props.theme === '2018' ?  {}: {color: 'rgb(248, 205, 6)'}} className="column col-6" key={news.id}>
                <div style={this.props.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)'}} className="card">
                    <div className="card-body">
                        <h1>{news.title}</h1>
                    </div>
                    <div className="card-footer">
                        <Link className={this.props.theme === '2018' ? "button-view" : 'Dark-button-view'} to={`${news.id}/`}>Читать</Link>
                    </div>
                </div>
            </div>
		);
	};

    compare = (a, b) => {
        if (a.id < b.id) {
            return 1;
        }
        if (a.id > b.id) {
            return -1;
        }
    };

	render(){
		this.posts_types = [];
		const news = this.props.news;
		this.posts = news;
        news.sort(this.compare);
		return (
			<div className="columns">
				{news.map(this.renderNews.bind(this))}
			</div>
		)
	}
}

export default connect(null,{deletePost,changeMode})(News);