import React,{Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Field,reduxForm} from 'redux-form';
import {renderInput} from '../../utils/redux-form-fields';
import {signin, get_nowuser} from '../../actions/Authentication/index';
import {VK_Bot} from '../../actions/index'

import '../../css/signin.css'

class Signin extends Component{

	formSubmit(formValue) {
        this.props.signin(formValue, () => {
        	//window.location.href='/posts/';
        })
    }
    componentWillMount(){
        this.props.theme.theme = localStorage.getItem('theme');
    }

	render(){
		const {handleSubmit} = this.props;
		const {loginError} = this.props.auth;
		return(
			<div style = {this.props.theme.theme === '2018' ?  {}: {color: 'rgb(248, 205, 6)'}} className="columns columns-signin">
				<div className="column">
					<form onSubmit={handleSubmit(this.formSubmit.bind(this))}>
						<Field component={renderInput} type="text" name="username" label="Имя пользователя"/>
						<Field component={renderInput} type="password" name="password" label="Пароль"/>
						<div className="form-group">
							{loginError?(<div className="form-group"><span className="label label-error">{loginError}</span></div>):""}
							<button type="submit" className={this.props.theme.theme === '2018' ? "button-view" : 'Dark-button-view'}>Войти</button>
						</div>
					</form>
				</div>
	            <div className="column col-5">
	            </div>

			</div>
		);
	}
}

Signin = withRouter(Signin);

Signin = reduxForm({
	form:'SigninForm',
	fields:['username','password']
})(Signin);

function mapStateToProps(state){
	return{
		auth: state.auth,
        theme: state.theme
	}
}

export default connect(mapStateToProps,{signin, get_nowuser, VK_Bot})(Signin);