import React,{Component} from "react";
import {Link} from 'react-router-dom';

import '../../css/profile.css'

class PostsBy extends Component{

	renderPost = (post) => {
		if (this.props.user.username === post.author) {
		    console.log('BUG', this.href_respondent);
			return(
			    <div>
                    <div className="column-post" key={post.id}>
                        <div style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)'}} className="card">
                            <div className="Data-post-title">
                            Заголовок: {post.title}
                          </div>
                          <div className="Data-post">
                            Текст: {post.content}
                          </div>
                            <div className="Data-post">
                                Тип:  {post.type}
                            </div>
                            <div className="Data-post">
                                Ответчик:
                                <a href={`/posts/view_post/${post.respondent}`}> {post.respondent}</a>
                            </div>
                          <div className="Data-post-button">
                            <a className={this.props.theme.theme === '2018' ? "button-view" : 'Dark-button-view'} href={`/posts/view_post/${post.id}`}>Посмотреть</a>
                          </div>
                        </div>
                    </div>
                </div>
			);
		}
	};

	render(){
		this.href_respondent = [];
		const posts = this.props.posts;
		console.log('A', posts, this.props.user.username);
		return (
			<div style = {this.props.theme.theme === '2018' ?  {}: {color: 'rgb(248, 205, 6)'}}>
                <div style={{display: 'none'}} id='MEM'>
				    {posts.map(this.renderPost.bind(this))}
                </div>
			</div>
		)
	}
}

export default PostsBy;