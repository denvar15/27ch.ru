import React,{Component,PropTypes} from "react";
import {connect} from "react-redux";
import {deletePost} from "../../actions/index";
import {imagePostUser} from '../../actions/Authentication/index'
import PostsBy from './posts_by'
import {Field,reduxForm} from "redux-form";
import '../../css/profile.css'
import FieldFileInput from "../create_post/image_adding/fileUpload";
import bagPhoto from '../../../images/splash.jpg'

class Profile extends Component{
	onSubmit(formValue){
		console.log(formValue.image);
		this.props.imagePostUser(formValue.image, this.props.user.UID, ()=>{
			 //this.props.history.push("/posts/");
		})
	}

	render(){
		const {fields:{title,content},handleSubmit} = this.props;
	    const posts = this.props.posts;
	    console.log(this.props.user);
	    if (this.props.user.type === 'moder') {
	    	 this.typer = this.props.theme.theme === '2018' ? 'Moder-class' : 'Dark-Moder-class'
		} else {
	    	 this.typer = this.props.theme.theme === '2018' ? "Common-class" : 'Dark-Common-class'
		}
		return(
			<div style = {this.props.theme.theme === '2018' ?  {}: {color: 'rgb(248, 205, 6)', background: 'rgb(33, 34, 38)'}} className="column col-12" key={this.props.user.username}>
				<div className='column col-12'>
					<form  onSubmit = {handleSubmit(this.onSubmit.bind(this))}>
					<div style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)'}} className="Data">
						<img width='100px' height='100px' src={this.props.user.image !== "https://django-tablet.s3.amazonaws.com/media/uploads/photo.jpg" ? this.props.user.image: bagPhoto}/>
						<Field component={FieldFileInput} name='image'/>
						<button type='sumbit' className={this.props.theme.theme === '2018' ? "button-view" : 'Dark-button-view'}>Загрузить</button>
					</div>
					</form>
					<div style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)'}} className="Data">
						<div className="Data-user">
							Имя пользователя: {this.props.user.username}
					  </div>
					  <div className={this.typer}>
						   Тип: {this.props.user.type}
					  </div>
						<div className="Data-user">
							Почта:  {this.props.user.email}
						</div>
					</div>
					<div className="column col-11">
                        <button className={this.props.theme.theme === '2018' ? 'button-rotor' : 'Dark-button-rotor'} onClick={() => {const e = document.getElementById("MEM");
                            if (e.style.display === 'none') {
                                e.style.display = 'inline'
                            }else {
                                e.style.display = 'none'
                            }}}><i className="fas fa-angle-down"> </i> Ваши посты
                        </button>
						<div style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)'}} className="card">
							<PostsBy id="MEM" theme={this.props.theme} posts={this.props.posts} user={this.props.user}/>
						</div>
					</div>
				</div>
            </div>
		);
	}
}

//redux form almost similar to connect 1st parameter is form object 2nd is mapStateToProps 3rd is mapDispatchToProps.
//we will use shorthand of mapDispatchToProps ,We could have written mapDispatchToProps function and then use bindActionCreators to map dispatch to props .but instead of doing all that we just pass the function as argument in reduxForm.

function mapStateToProps(state){
	return {
		auth:state.auth,
	}
}

export default connect(mapStateToProps, {deletePost, imagePostUser})(reduxForm({
	form:'UserUpdateForm',
	fields:['image'],
})(Profile));