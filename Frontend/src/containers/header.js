import React,{Component} from 'react';
import {Link,Route,withRouter} from 'react-router-dom'
import {connect} from 'react-redux';

import '../css/header.css'
import logo from '../../images/27-spring18.jpg'
import Darklogo from '../../images/uJNe8Zbm25A.jpg'

import Blogs from "./types/types_index";
import PostNew from "./create_post/post_new";
import ViewPost from './post_detail/view_post';
import EditPost from './post_detail/edit_post';
import Signup from './auth/signup_form';
import Signin from './auth/signin_form';
import Posts from './posts/posts_index'
import Profiles from './profile/profile_index'
import NewsView from './news/news_index'
import SingleNewsView from './news/news_detail_index'
import DropZone from './create_post/image_adding/fileUpload'
import LastPosts from './last_posts/types_index'

import requireAuth from './HOC/authenticate';

import {signout} from '../actions/Authentication';
import {resetTheme} from '../actions/index'

class Header extends Component{
	logoutUser(){
		this.props.signout(()=>{
			this.props.history.push('/signin');
		})
	}
	renderAuthMode(authenticated){
		if(authenticated){
			return(
				<section className="navbar-section">
                    <Link to="/profile" className="btn btn-link"><p className={this.props.theme.theme === '2018' ? 'Links-text' : 'Dark-Links-text'}>Профиль</p></Link>
                    <a className="btn btn-link" onClick={this.logoutUser.bind(this)}><p className={this.props.theme.theme === '2018' ? 'Links-text' : 'Dark-Links-text'}>Выйти</p></a>
				</section>
			);
		}
		return(
			<section className="navbar-section">
                <Link to="/signup" className="btn btn-link"><p className={this.props.theme.theme === '2018' ? 'Links-text' : 'Dark-Links-text'}>Регистрация</p></Link>
                <Link to="/signin" className="btn btn-link"><p className={this.props.theme.theme === '2018' ? 'Links-text' : 'Dark-Links-text'}>Вход</p></Link>
			</section>
		);

	}
	componentWillMount(){
        this.props.theme.theme = localStorage.getItem('theme');
    }
	render(){
		const {authenticated} = this.props;
		return(
		    <div >
                <div style={this.props.theme.theme === '2018' ? {minHeight:'100%'} : {minHeight:'100%', background: 'rgb(33, 34, 38)', color: ' color: rgb(248, 205, 6)'}}  className="contianer">
                    <div className="container">
                        <div className="columns">
                            <div className={this.props.theme.theme === '2018' ? 'NavigBar' : 'Dark-NavigBar'}>
                                <div className="column col-lg-12">
                                    <header className="navbar">
                                        <a className="navbar-logo">
                                            <a href='/'><img src={this.props.theme.theme === '2018' ? logo : Darklogo} height='50px' width='50px'/></a>
                                        </a>
                                        <section className="navbar-section">
                                            {authenticated?(<Link to="/" className="btn btn-link"><p className={this.props.theme.theme === '2018' ? 'Links-text' : 'Dark-Links-text'}>Последние посты</p></Link>):""}
                                            {authenticated?(<Link to="/posts/" className="btn btn-link"><p className={this.props.theme.theme === '2018' ? 'Links-text' : 'Dark-Links-text'}>Темы</p></Link>):""}
                                            {authenticated?(<Link to="/news/" className="btn btn-link"><p className={this.props.theme.theme === '2018' ? 'Links-text' : 'Dark-Links-text'}>Новости сайта</p></Link>):""}
                                            {authenticated?(<Link to="/posts/create_post" className="btn btn-link"><p className={this.props.theme.theme === '2018' ? 'Links-text' : 'Dark-Links-text'}>Написать пост</p></Link>):""}
                                        </section>
                                        <div className="btn btn-link"><p className={this.props.theme.theme === '2018' ? 'Version-text' :'Dark-Version-text'} >Betta 1.7 (bot vk всё ещё минус)</p></div>
                                        <a className='ProfileClick' onClick={() => {
                                            const e = document.getElementById("Profile");
                                            const a = document.getElementById('Profile-arrow');
                                            if (e.style.display === 'none') {
                                                e.style.display = 'inline';
                                                a.className = 'fas fa-arrow-right fa-3x';
                                            }else {
                                                e.style.display = 'none';
                                                a.className = 'fas fa-arrow-left fa-3x';
                                            }}}><i id='Profile-arrow' style={this.props.theme.theme === '2018' ? {color: 'white'}: {color: 'rgb(248, 205, 6)'}} className="fas fa-arrow-right fa-3x"> </i></a>
                                        <div id='Profile'>
                                            {this.renderAuthMode(authenticated)}
                                            </div>
                                        </header>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style={{minHeight:'100vh', minWidth:'100vw'}}>
                        <Route exact path="/" component={requireAuth(LastPosts)}/>
                        <Route exact path="/posts/" component={requireAuth(Blogs)}/>
                        <Route path="/profile" component={requireAuth(Profiles)}/>
                        <Route exact path="/posts" component={requireAuth(Blogs)}/>
                        <Route path = "/posts/:id" component = {requireAuth(Posts)}/>
                        <Route path = "/signup" component ={Signup}/>
                        <Route path = "/signin" component ={Signin}/>
                        <Route exact path = "/posts/create_post" component= {requireAuth(PostNew)}/>
                        <Route path = '/posts/create_post/image' component={requireAuth(DropZone)}/>
                        <Route path = "/posts/view_post/:id" component = {requireAuth(ViewPost)}/>
                        <Route path = "/posts/edit_post/:id" component = {requireAuth(EditPost)}/>
                        <Route exact path="/news" component={requireAuth(NewsView)}/>
                        <Route path="/news/:id" component={requireAuth(SingleNewsView)}/>
                    </div>
                    <footer className={this.props.theme.theme === '2018' ? 'Footer' : 'Dark-Footer'}>
                        <div>
                            <p className={this.props.theme.theme === '2018' ? "Company" : "Dark-Company"}>Студия - 'Я вообще то один это писал! (Денис Сучков)</p>
                            <a className={this.props.theme.theme === '2018' ? "VK" : "Dark-VK"}  href='https://vk.com/id138089842'> Мой VK</a>
                            <a  className={this.props.theme.theme === '2018' ? "Links-text-reverse" : "Dark-Links-text-reverse"} onClick={() => {this.props.resetTheme(this.props.theme.theme); console.log(this.props.theme.theme)}}>Сменить тему</a>
                        </div>
                    </footer>
                </div>
            </div>
		);
	}
}

function mapStateToProps(state){
	return{
		authenticated:state.auth.authenticated,
        auth: state.auth,
        theme: state.theme
	}
}

export default withRouter(connect(mapStateToProps,{signout, resetTheme})(Header));

