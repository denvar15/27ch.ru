import React,{Component} from "react";

import '../../css/posts.css'

class Posts extends Component{

	go_detail = (a, post) => {
        window.location.href = `/posts/view_post/${a.id}`
	};

    go_respondentor = (a, post) => {
        window.location.href = `/posts/${this.href_respondent[a.id]}`
    };

	renderPost = (post) => {
		if (post.respondent === 'no respondent') {
			 this.href_respondent[post.id] = post.type + '/'
		} else {
			this.href_respondent[post.id] = 'view_post/' + post.respondent
		}
			return(
			<div className="column col-12" key={post.id}>
				<div style={this.props.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)'}} className="card">
                    <div className="card-body">
				    Заголовок: {post.title}
				  </div>
				  <div className="card-body">
				    Текст: {post.content}
				  </div>
					<div className="card-body">
						Тип:  {post.type}
				    </div>
					<div className="card-body">
						Ответчик:
                        <button className={this.props.theme === '2018' ? "button-view" : 'Dark-button-view'} onClick={this.go_respondentor.bind(this, post)}> {post.respondent}</button>
					</div>
				  <div className="card-footer">
				    <button className={this.props.theme === '2018' ? "button-view" : 'Dark-button-view'} onClick={this.go_detail.bind(this, post)}>Смотреть</button>
				  </div>
				</div>
			</div>
			);

	};

    compare = (a, b) => {
        if (a.id < b.id) {
            return 1;
        }
        if (a.id > b.id) {
            return -1;
        }
    };

    render(){
		this.href_respondent = [];
		const posts = this.props.posts;
        posts.sort(this.compare);
        return (
			<div style = {this.props.theme === '2018' ?  {}: {color: 'rgb(248, 205, 6)'}} className="columns">
				{posts.map(this.renderPost.bind(this))}
			</div>
		)
	}
}

export default Posts;