import React,{Component} from "react";
import {Link} from 'react-router-dom';
import {changeMode, deletePost} from '../../actions/index';
import {connect} from "react-redux";

import '../../css/typelist.css'

class Posts extends Component{

	find = (array, value) => {
		for (let i = 0; i < array.length; i++) {
			if (array[i] === value)
				return i;
		}
		return -1;
	};

	count = (array, ob) => {
		console.log('counter', array, ob);
        let count = 0;
        for (let i = 0; i < array.length; ++i) {
            if (array[i] === ob)
                count++;
        }
        return count
    };


	delete = (theme) => {
		const posts = this.props.posts;
		let counter = 0;
		console.log(this.props.user);
		if (this.props.user.type === 'moder') {
			posts.map((post) => {
				if (theme.type === post.type) {
					counter++;
					this.props.deletePost(post.id, () => {
						if (counter === this.count(this.posts_types, theme.type))
						{
							window.location.href = "/posts/";
						}
					}
					)
				}
			}
			)
		} else {
		    window.location.href = "/posts/";
        }
	};

	renderDeleteButton(post){
		if(this.deletebutton === true){
			return (
				<button className={this.props.theme.theme === '2018' ? "button-delete" : "button-delete-dark"} onClick={this.delete.bind(null, post)}>Удалить тему</button>
			)
		} else {
		    return (
		        <div> </div>
            )
        }
	}

	renderPost = (post) => {
		this.posts_types.push(post.type);
		if (this.find(this.typelist, post.type) === -1) {
			this.typelist.push(post.type);
			return(
			<div style = {this.props.theme.theme === '2018' ?  {}: {color: 'rgb(248, 205, 6)'}} className="column col-6" key={post.id}>
				<div style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)'}} className="card">
					<div className="card-body">
                        <h1>{post.type}</h1>
				    </div>
				  <div className="card-footer">
                      <Link className={this.props.theme.theme === '2018' ? "button-view" : "Dark-button-view"} to={`/posts/${post.number_type}/`}>Смотреть тему</Link>
				  </div>
					<div className="card-footer">
                        {this.renderDeleteButton(post)}
					</div>
				</div>
			</div>
		);
		}
	};

	compare = (a, b) => {
        if (a.id < b.id) {
            return 1;
        }
        if (a.id > b.id) {
            return -1;
        }
    };

	render(){
	    this.deletebutton = false;
	    if (this.props.user !== null) {
	        if (this.props.user.type === 'moder') {
	            this.deletebutton = true;
	        }
        }
		this.posts_types = [];
		this.typelist = [];
		const posts = this.props.posts;
		this.posts = posts;
		posts.sort(this.compare);
		console.log("sorted posts", posts);
		return (
			<div className="columns">
				{posts.map(this.renderPost.bind(this))}
				</div>
		)

	}
}

export default connect(null,{deletePost,changeMode})(Posts);