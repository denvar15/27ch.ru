import React,{Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {createPost, deletePost} from '../../actions/index';
import {changeMode} from '../../actions/index';
import bagPhoto from '../../../images/splash.jpg'
import noPhoto from '../../../images/images.png'
import moment from 'moment';
import {Field,reduxForm} from "redux-form";

import '../../css/postdetail.css'

class PostDetail extends Component{
	delete(){
		const {data} = this.props.data.post;
		this.props.deletePost(data.id,()=>{
			this.props.data.history.push("/posts/");
		});
	}

	renderEditButton(){
		const {data} = this.props.data.post;
		const owner = data.author;
		const requestUser = localStorage.getItem('username');
		if(owner==requestUser){
			return (
				<Link className={this.props.theme.theme === '2018' ? "button-back" : 'Dark-button-back'} to={`/posts/edit_post/${data.id}`}>Редактировать</Link>
			)
		}
	}
	renderDeleteButton(){
		const {data} = this.props.data.post;
		const owner = data.author;
		const requestUser = localStorage.getItem('username');
		if(owner==requestUser){
			return (
				<button className={this.props.theme.theme === '2018' ? "button-back" : 'Dark-button-back'} onClick={this.delete.bind(this)}>Удалить</button>
			)
		}
	}

	onSubmit(formValue, data){
	    data.type = formValue.type;
	    data.respondent = formValue.id ;
	    data.PID = this.props.now_user.UID;
	    console.log(this.props.now_user);
		this.props.createPost(data,()=>{
		    console.log(formValue, data);
		    window.location.href = "/posts/";
		})
	}

	renderPost = (post, data) => {
		if (post.respondent === 'no respondent') {
			 this.href_respondent[data.id] = data.type + '/'
		} else {
			this.href_respondent[data.id] = 'view_post/' + post.respondent
		}
		if (post.id  == data.respondent) {
			return(
			<div className="column col-12" key={data.id}>
				<div style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)'}} className="card">
                    <div className="card-body">
				    Title: {data.title}
				  </div>
				  <div className="card-body">
				    Content: {data.content}
				  </div>
				  <div className="card-footer">
				    <Link className={this.props.theme.theme === '2018' ? "button-view" : 'Dark-button-view'} to={`/posts/view_post/${data.id}`}>Смотреть</Link>
				  </div>
				</div>
			</div>
			);
		}
	};

	render(){
		const {fields:{title,content},handleSubmit} = this.props;
		const {data} = this.props.data.post;
		window.data = data;
		const time = moment(data.published).format("MMM Do YY");

		const posts = this.props.posts;
		this.href_respondent = [];
		return (
			<div style = {this.props.theme.theme === '2018' ?  {}: {color: 'rgb(248, 205, 6)'}} className="panel">
				<div style = {this.props.theme.theme === '2018' ?  {}: {color: 'rgb(248, 205, 6)'}} className="panel-header">
                    <div style = {this.props.theme.theme === '2018' ?  {color:'#53575f'}: {color: 'rgb(248, 205, 6)'}} className="panel-subtitle float-right">Posted:-{time}</div>
                    <div className="panel-title h5 mt-10">{data.title}</div>
					<img style={{display:'block'}}  width="50px" height="50px" src={this.props.user.image !== "https://django-tablet.s3.amazonaws.com/media/uploads/photo.jpg" ? this.props.user.image: bagPhoto} />
					<div style = {this.props.theme.theme === '2018' ?  {display:'inline-block', marginLeft:'55px', marginTop:'-35px', color:'#53575f'}: {color: 'rgb(248, 205, 6)'}}  >Автор:-{data.author}</div>
					<div style={this.props.theme.theme === '2018' ?  {color:'#53575f'}: {color: 'rgb(248, 205, 6)'}}>Тема:  {data.type}</div>
					<div style={this.props.theme.theme === '2018' ?  {color:'#53575f'}: {color: 'rgb(248, 205, 6)'}}>Ответчик:  {data.respondent}</div>
				</div>
				<div className="panel-body">
				   <p>{data.content}</p>
				</div>
				<div className="panel-body">
                    <img style={{display:'block', maxWidth:'100px', maxHeight:'100px'}} src={data.image !== null ? data.image: noPhoto}/>
				</div>
				<div className="panel-footer">
				  	<div className="btn-group btn-group-block">
                   		{this.renderEditButton()}
                   		{this.renderDeleteButton()}
                    <Link className={this.props.theme.theme === '2018' ? "button-back" : 'Dark-button-back'} to="/posts/">Назад</Link>
                  </div>
					<div className="panel-footer">
						<div className="columns">
							<div className="column col-12">
                                <button className={this.props.theme.theme === '2018' ? 'button-rotor' : 'Dark-button-rotor'} onClick={() => {const e = document.getElementById("LOL");
                                        if (e.style.display === 'none') {
                                            e.style.display = 'inline'
                                        }else {
                                            e.style.display = 'none'
                                        }}}><i className="fas fa-angle-down"> </i> Ответить
                                </button>

                                <form style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)'}} className='card' onSubmit = {handleSubmit(this.onSubmit.bind(this, data))}>
                                    <ul id='LOL' style={{display: 'none'}}>
                                        <div className="form-group">
                                            <label className="form-label">Заголовок</label>
                                            <Field style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)', color: 'rgb(240, 244, 244)'}} component="input"  name="title" type="text" className="form-input" placeholder="Enter the title of the Post"/>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Сообщение</label>
                                            <Field style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)', color: 'rgb(240, 244, 244)'}} component="textarea" name="content" className="form-input" id="input-example-3" placeholder="Textarea" rows="3"/>
                                        </div>
                                        <div className="form-group">
                                            <button className={this.props.theme.theme === '2018' ? "button-view" : 'Dark-button-view'} type="submit" id="vini">Отправить</button>
                                            <Link to="/posts/" className={this.props.theme.theme === '2018' ? "button-back" : 'Dark-button-back'}> Отмена</Link>
                                        </div>
                                    </ul>
                                </form>
                            </div>
						</div>
                        <button className={this.props.theme.theme === '2018' ? 'button-rotor' : 'Dark-button-rotor'} onClick={() => {const e = document.getElementById("MEM");
                                        if (e.style.display === 'none') {
                                            e.style.display = 'inline'
                                        }else {
                                            e.style.display = 'none'
                                        }}}><i className="fas fa-angle-down"> </i> Ответы
                                </button>
                        <div id='MEM' className="columns" style={{display: 'none'}}>
                            {posts.map(this.renderPost.bind(this, data))}
                        </div>
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state){
	return {
		newpost:state.newpost,
	}
}

export default connect(mapStateToProps, {deletePost,changeMode, createPost})(reduxForm({
	form:'PostForm',
	fields:['title','content', 'type' , 'respondent'],
})(PostDetail));