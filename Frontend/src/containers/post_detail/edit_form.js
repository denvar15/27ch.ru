import React,{Component} from 'react';
import {connect} from 'react-redux';
import {Field,reduxForm} from 'redux-form';
import {Link,withRouter} from 'react-router-dom';
import {renderInput} from '../../utils/redux-form-fields';
import {editPost} from '../../actions/index';

import '../../css/postdetail.css'

class EditForm extends Component{
	formSubmit(formValue){
		const {data} = this.props.data;
		//console.log(data.id);
		formValue.type = renderInput.type;
		this.props.editPost(formValue,data.id,()=>{
			window.location.href = "/posts/";
		});
	}
	componentDidMount() {
		const {data} = this.props.data;
		this.props.initialize({
			title:data.title,
			content:data.content
		});
	}

    go_detail = (a, data) => {
        window.location.href = `/posts/view_post/${a.id}`
    };

	render(){
		const {handleSubmit} = this.props;
		const {data} = this.props.data;
		//console.log(this.props);
		return(
			<form style = {this.props.theme.theme === '2018' ?  {}: {color: 'rgb(248, 205, 6)'}} onSubmit={handleSubmit(this.formSubmit.bind(this))}>
				<Field component={renderInput}
					label="Title"
					type = "text"
					name = "title"
				/>
				<Field style={this.props.theme.theme === '2018' ? {} : {background: 'rgb(33, 34, 38)', color: 'rgb(240, 244, 244)'}} component={renderInput}
				label="Content"
				type = "text"
				name = "content"
			/>
				<div className="form-group">
				 	<button className={this.props.theme.theme === '2018' ? "button-view" : 'Dark-button-view'} type="submit">Сохранить</button>

                    <button className={this.props.theme.theme === '2018' ? "button-back" : 'Dark-button-back'} onClick={this.go_detail.bind(this, data)}>Отмена</button>
				 </div>
			</form>
		);
	}
}

function mapStateToProps(state){
    return {
        theme: state.theme
    }
}

EditForm = withRouter(EditForm);
EditForm = reduxForm({
	form:'EditForm',
	fields:['title','content', 'type'],
})(EditForm);


export default connect(mapStateToProps,{editPost})(EditForm);