import React,{Component} from 'react';
import {connect} from 'react-redux';
import {viewPost} from '../../actions/index';

import Loading from "../../components/loading";
import PostDetail from "./postdetail";
import {getBlogs} from "../../actions/index";
import {get_post_nowuser, get_nowuser} from '../../actions/Authentication/index'

class ViewPost extends Component{

	componentDidMount() {
        if (this.props.post.isFetching === false && this.props.post.isFetched === false) {
            const {id} = this.props.match.params;
            this.props.viewPost(id, ()=>{
                getBlogs();
                this.props.get_nowuser();
                this.props.get_post_nowuser(this.props.post.data.PID);
			});
		}
	}
    componentWillMount(){
        this.props.theme.theme = localStorage.getItem('theme');
    }

    renderall(koef, isFetched, isFetchedB){
        if (koef === true && isFetchedB && isFetched) {
            return(
                <div className="container">
                    {<PostDetail theme={this.props.theme} now_user={this.props.auth.user} user={this.props.auth.user_post} data={this.props} posts={this.props.blogs.posts}/>}
                </div>
            );
        } else {
            return (
                <div className="container">
                    <Loading/>
                </div>
            )
        }
    }

	render(){
		const {isFetching,isFetched} = this.props.post;
        const isFetchingB = this.props.blogs.isFetching;
        const isFetchedB = this.props.blogs.isFetched;
        let mem = false;
        if (this.props.auth.fetched_post) {
            if (this.props.auth.user_post.username === this.props.post.data.author) {
            	mem = true;
            }
		}
        console.log('A', this.props.auth.user_post, this.props.post.data);
		return(
		    <div className="container">
                {this.renderall(mem, isFetched, isFetchedB)}
		    </div>
		)
	}
}

function mapStateToProps(state){
	return{
	    post: state.post,
	    blogs:state.blogs,
        auth:state.auth,
        theme: state.theme
	}
}



export default connect(mapStateToProps, {viewPost, get_post_nowuser, get_nowuser})(ViewPost);