from django.conf import settings
from django.db import models


class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default=None)
    respondent = models.CharField(max_length=100, default='no respondent')
    type = models.CharField(max_length=100, default='flud')
    number_type = models.IntegerField(default=0)
    content = models.TextField(max_length=200, default=None)
    published = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='uploads', default=None, blank=True, null=True)
    PID = models.IntegerField(default=0, null=True)

    def __str__(self):
        return str(self.title)
