from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField
)
from .models import Post


class PostCreateSerializer(ModelSerializer):
    author = SerializerMethodField()

    class Meta:
        model = Post
        fields = ['title', 'respondent', 'type', 'number_type', 'content', 'author', 'published', 'image', 'PID']

    @staticmethod
    def get_author(obj):
        return str(obj.author.username)

    def create(self, validated_data):
        maximum = 0
        finder = 0
        for j in Post.objects.all():
            if j.type == validated_data['type']:
                finder = 1
        if finder != 1:
            for i in Post.objects.all():
                if i.number_type > maximum:
                    maximum = i.number_type
            number_type = maximum + 1
        else:
            obj = Post.objects.filter(type=validated_data['type'])[0]
            number_type = obj.number_type
        post = Post.objects.create(
            title=validated_data['title'],
            respondent=validated_data['respondent'],
            type=validated_data['type'],
            number_type=number_type,
            content=validated_data['content'],
            author=validated_data['author'],
            PID=validated_data['PID'],
        )
        post.save()
        return post


class PostDetailSerializer(ModelSerializer):
    author = SerializerMethodField()

    class Meta:
        model = Post
        fields = ['id', 'title', 'respondent', 'type', 'number_type',
                  'content', 'published', 'author', 'published', 'image', 'PID']
        lookup_field = 'pk'

    @staticmethod
    def get_author(obj):
        return str(obj.author.username)


class PostListSerializer(ModelSerializer):
    author = SerializerMethodField()

    class Meta:
        model = Post
        fields = ['id', 'title', 'respondent', 'type', 'number_type', 'content', 'author', 'published', 'image', 'PID']

    @staticmethod
    def get_author(obj):
        return str(obj.author.username)

