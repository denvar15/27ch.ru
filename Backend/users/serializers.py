from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
)
from rest_framework_jwt.settings import api_settings

from .models import CustomUser, Moder, NowUser


class UserCreateSerializer(ModelSerializer):
    token = SerializerMethodField()

    class Meta:
        model = CustomUser
        fields = [
            'email',
            'username',
            'password',
            'token',
            'type',
            'image',
            'id'
        ]
        extra_kwargs = {"password": {"write_only": True}}

    @staticmethod
    def get_token(request):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(request)
        token = jwt_encode_handler(payload)
        return token

    def create(self, validated_data):
        typer = 'common'
        maximum = -1
        if str(Moder.objects.all()).find(validated_data['username']) != -1:
            typer = 'moder'
        for i in CustomUser.objects.all():
            if int(i.id) > maximum:
                maximum = int(i.id)
        maximum += 1
        user = CustomUser.objects.create(
            email=validated_data['email'],
            username=validated_data['username'],
            type=typer,
            id=str(maximum),
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class UserSerializer(ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('username', 'type', 'email', 'image', 'id')
        error_css_class = 'error'


class ModerSerializer(ModelSerializer):
    class Meta:
        model = Moder
        fields = [
            'name',
            'type',
        ]


class NowUserCreateSerializer(ModelSerializer):
    class Meta:
        model = NowUser
        fields = [
            'username',
            'type',
            'email',
            'image',
            'UID'
        ]

    def create(self, validated_data):
        typer = 'common'
        if str(Moder.objects.all()).find(validated_data['username']) != -1:
            typer = 'moder'
        user = NowUser.objects.create(
            username=validated_data['username'],
            type=typer,
            image=validated_data['image'],
            email=validated_data['email'],
            UID=validated_data['UID'],
        )
        user.save()
        return user


class NowUserSerializer(ModelSerializer):
    class Meta:
        model = NowUser
        fields = [
            'username',
            'type',
            'email',
            'image',
            'UID'
        ]
