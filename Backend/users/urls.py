from django.conf.urls import url

from .views import (
    UserCreateView,
    NowUserCreate,
    NowUserView,
    UserUpdateView,
    NowUserUpdate
)

from rest_framework_jwt.views import obtain_jwt_token

app_name = 'users'

urlpatterns = [
    url(r'^register/$', UserCreateView.as_view(), name='users'),
    url(r'^home/login/token/$', obtain_jwt_token),
    url(r'^create/now_user/$', NowUserCreate.as_view()),
    url(r'^update/now_user/(?P<pk>[\d-]+)/$', NowUserUpdate.as_view()),
    url(r'^get/now_user/(?P<pk>[\d-]+)/$', NowUserView.as_view()),
    url(r'^update/(?P<pk>[\d-]+)/$', UserUpdateView.as_view(), name='Update'),
]
