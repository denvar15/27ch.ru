from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models


class CustomUserManager(UserManager):
    pass


class CustomUser(AbstractUser):
    image = models.ImageField(upload_to='uploads', default='uploads/photo.jpg', blank=True, null=True)
    type = models.CharField(max_length=50, default='common')
    id = models.CharField(primary_key=True, default='0', editable=True, max_length=100000)
    objects = CustomUserManager()


class Moder(models.Model):
    type = models.CharField(max_length=100, default='common')
    name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.name)


class NowUser(models.Model):
    image = models.CharField(default=None, blank=True, null=True, max_length=100000)
    type = models.CharField(max_length=100, default='common')
    username = models.CharField(max_length=100)
    email = models.EmailField(default='ex@ex.ex')
    UID = models.CharField(default=0, max_length=100000, editable=True, primary_key=True)

    def __str__(self):
        return str(self.username)
